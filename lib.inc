section .text
%define NEWLINE_CHAR 0xA
%define ASCII_ZERO "0"
%define MINUS_SYMBOL "-"
%define PLUS_SYMBOL "+"
%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9
%define SYS_EXIT 60
%define SYS_READ 0
%define SYS_WRITE 1
%define STDOUT 1
%define STDIN 0
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rax], 0
    je   .end
    inc  rax
    jmp  .counter
  .end:
    sub  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    
	pop rsi
    mov  rdx, rax
    mov  rax, SYS_WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jnl print_uint
	
  .negative:
	push rdi
	
	mov rdi, MINUS_SYMBOL
	call print_char
	
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    mov r8, rsp
	sub rsp, 32
	
	dec r8
	mov [r8], byte 0
    
  .loop:
    xor rdx, rdx
    div rcx
    
    add dl, ASCII_ZERO
	dec r8
	mov [r8], dl
    
    test rax, rax
    jnz .loop
  .end:
    
    mov rdi, r8
	call print_string
	
	add rsp, 32
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov rax, 1
	
  .loop:
	mov dl, byte[rdi]	
	cmp dl, byte[rsi]
	jnz .not_equal
	
	test dl, dl
	jz .end
	
	inc rdi
	inc rsi
	jmp .loop
	
  .not_equal:
	xor rax, rax
	
  .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, 8
	mov byte[rsp], 0
	
	mov rax, SYS_READ
	mov rdi, STDIN
	mov rsi, rsp
	mov rdx, 1
    syscall

	test rax, rax
	jng .fail
	
	mov al, [rsp]
	add rsp, 8
	ret

  .fail:
	xor rax, rax
	add rsp, 8
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
	push r13
	push rdi

	mov r12, rdi
	mov r13, rsi
	
	test rsi, rsi
	jz .end_fail
	
  .skip_spaces_loop:
	call read_char
	
	test rax, rax
	jng .end_fail
	
	cmp rax, SPACE_CHAR
	je .skip_spaces_loop
	cmp rax, TAB_CHAR
	je .skip_spaces_loop
	cmp rax, NEWLINE_CHAR
	je .skip_spaces_loop
	
	jmp .process_char
  
  .read_word_loop:
	call read_char
	
	test rax, rax
	jng .end_success
	
	cmp rax, SPACE_CHAR
	je .end_success
	cmp rax, TAB_CHAR
	je .end_success
	cmp rax, NEWLINE_CHAR
	je .end_success

  .process_char:
	cmp r13, 1
	jle .end_fail
	
	mov byte[r12], al
	inc r12
	dec r13
	jmp .read_word_loop
  
  .end_success:
	mov byte[r12], 0
	
	pop rax
	mov rdx, r12
	sub rdx, rax
	pop r13
	pop r12
    ret
  
  .end_fail:
	pop rax
	xor rax, rax
	xor rdx, rdx
	pop r13
	pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rcx, rcx
	xor rdx, rdx

	mov dl, byte[rdi]
	cmp dl, PLUS_SYMBOL
	jne .loop
	inc rcx
	
  .loop:
	mov dl, byte[rdi + rcx]
	sub dl, ASCII_ZERO
	
	cmp dl, 0
	js .end
	cmp dl, 10
	jns .end
	
	imul rax, 10
	add rax, rdx
	inc rcx
	
	jmp .loop
	
  .end:
	mov rdx, rcx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov dl, byte[rdi]
	cmp dl, MINUS_SYMBOL
	je .negative
	
  .positive:
	call parse_uint
    ret 

  .negative:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	
  .loop:
	mov dl, byte[rdi]
	mov byte[rsi], dl
	inc rdi
	inc rsi
	inc rax

	cmp rdx, rax
	jle .error
	
	cmp dl, 0
	jz .end
	
	jmp .loop
	
  .end:
	ret
    
  .error:
	xor rax, rax
	ret
